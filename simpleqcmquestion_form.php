<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* The main simpleqcm configuration form
*
* It uses the standard core Moodle formslib. For more info about them, please
* visit: http://docs.moodle.org/en/Development:lib/formslib.php
*
* @package    mod
* @subpackage simpleqcm
* @author     Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/mod/simpleqcm/lib.php');

/**
 * Module instance settings form
 */
class simpleqcmquestion_form extends moodleform {

    protected function definition()
    {
        $mform = $this->_form;

        // **** Hidden fields
        $mform->addElement('hidden', 'questionid');
        $mform->setType('questionid', PARAM_INT);
        $mform->addElement('hidden', 'categoryid');
        $mform->setType('categoryid', PARAM_INT);
        $mform->addElement('hidden', 'contextid');
        $mform->setType('contextid', PARAM_INT);
        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);
        $mform->addElement('hidden', 'name');
        $mform->setType('name', PARAM_TEXT);

        // ***** Displayed fields
        $mform->addElement('editor', "questiontexteditor", get_string('questiontext', 'simpleqcm'),
            array('rows' => 3), array('maxfiles' => EDITOR_UNLIMITED_FILES,
                'noclean' => true, 'subdirs' => true));
        $mform->setType("question_texteditor", PARAM_RAW);

        for ($iA = 0 ; $iA < MAX_ANSWER_NB ; $iA++) {
            $answergroupname = "answer{$iA}group";
            $answeridelement = $this->answer_id_element($iA);
            $answertextelement = $this->answer_text_editor_element($iA);
            $answeriscorrectelement = $this->answer_is_correct_element($iA);

            $answerelements = [];
            $answerelements[] = $mform->createElement('editor', $answertextelement, '',
                array('rows' => 1), array('maxfiles' => EDITOR_UNLIMITED_FILES,
                    'noclean' => true, 'subdirs' => true,
                    'autosave' => false));
            $mform->setType($answertextelement, PARAM_TEXT);

            $answerelements[] = $mform->createElement('advcheckbox', $answeriscorrectelement,  '', null, [0, 1]);
            $mform->setType($answeriscorrectelement, PARAM_BOOL);

            $answerelements[] = $mform->createElement('hidden', $answeridelement);
            $mform->setType($answeridelement, PARAM_INT);
            $mform->setDefault($answeridelement, 0);

            $mform->addGroup($answerelements, $answergroupname,
                get_string('answer', 'simpleqcm') . ' ' . ($iA+1),
                ' ' . get_string('answeriscorrect', 'simpleqcm'), false);
        }

        // ****** Action buttons (only for compatibility and tests)
        $this->add_action_buttons();

    }

    /**
     * Load question from database to the form
     * @param int $questionid
     * @return stdClass form element object
     * @throws dml_exception
     */
    public function prepare_data(int $questionid, int $contextid): stdClass {
        global $DB;

        $data = new stdClass();

        if (empty($questionid)) return $data;

        $question = $DB->get_record('question', ['id' => $questionid], '*', MUST_EXIST);
        $category = $DB->get_record('question_categories', ['id' => $question->category], '*', MUST_EXIST);
        if ($contextid != $category->contextid) {
            throw new moodle_exception('Trying to illegally access to a question');
        }
        $contextid = $category->contextid;


        // Question
        $data->questionid = (int)$question->id;
        $data->name = $question->name;
        $data->categoryid = (int)$question->category;
        $data->questiontexteditor['format'] = $question->questiontextformat;
        $draftitemid = file_get_submitted_draft_itemid('questiontexteditor');
        $data->questiontexteditor['text'] = file_prepare_draft_area(
            $draftitemid, $contextid, 'question', 'questiontext',
            $questionid, null, $question->questiontext);
        $data->questiontexteditor['itemid'] = $draftitemid;

        // Answers
        $answers = $DB->get_records('question_answers', ['question' => $question->id]);
        $iA = 0;
        foreach ($answers as $answer) {
            $answertextelement = $this->answer_text_editor_element($iA);
            $answeridelement = $this->answer_id_element($iA);
            $answeriscorrectelement = $this->answer_is_correct_element($iA);

            $data->$answeridelement = (int)$answer->id;
            $draftitemid = file_get_submitted_draft_itemid($answertextelement);
            $data->$answertextelement['format'] = $answer->answerformat;
            $data->$answertextelement['text'] = file_prepare_draft_area(
                $draftitemid, $contextid, 'question', 'answer', $answer->id,
                null, $answer->answer);
            $data->$answertextelement['itemid'] = $draftitemid;

            $data->$answeriscorrectelement = $answer->fraction > 0;

            if (++$iA >= MAX_ANSWER_NB) {
                // TODO manage error case: too much answers
                break;
            }
        }

        return $data;
    }

    function answer_id_element($iA) { return "answer{$iA}id"; }
    function answer_text_editor_element($iA) { return "answer{$iA}editor"; }
    function answer_is_correct_element($iA) { return "answer{$iA}iscorrect"; }
}