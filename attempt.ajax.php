<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package    mod_simpleqcm
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use vikimodule\Question;

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/simpleqcm/locallib.php');
require_once($CFG->dirroot . '/mod/simpleqcm/lib.php');

$qcmid          = required_param('qcmid', PARAM_INT);
$simpleqcm      = $DB->get_record('simpleqcm', array('id' => $qcmid), '*', MUST_EXIST);
$course         = $DB->get_record('course', array('id' => $simpleqcm->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('simpleqcm', $simpleqcm->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);

$attempt          = required_param('attempt', PARAM_RAW);

require_login($course, false, $cm);

if (is_guest($context, $USER)) {
    throw new moodle_exception('noguestsubscribe', 'mod_simpleqcm');
}


if ($attempt == "null") {
  // QCM initialization
  $newAttempt = new stdClass;
  $newAttempt->simpleqcmid = $qcmid;
  $newAttempt->userid = $USER->id;
  $newAttempt->timestart = time();
  $newAttempt->timemodified = time();
  $newAttempt->timefinish = 0;
  $newAttempt->sequence = "";
  $newAttempt->score = 0;
  $newAttempt->courseid = $course->id;
  $id = $DB->insert_record('simpleqcm_attempts', $newAttempt);
  $_SESSION['simpleqcm_' . $qcmid] = $id;
  echo json_encode(array(
      'status'   => 'success',
      'contents' => 'initialize QCM',
  ));

}
else {
    // QCM in progress or finished
    if (isset($_SESSION['simpleqcm_' . $qcmid]) && $attempt = json_decode($attempt)) {
        $attemptid = $_SESSION['simpleqcm_' . $qcmid];
        $currentAttempt = $DB->get_record('simpleqcm_attempts', array('id'=>$attemptid));

        //Calculate questions, attempts, successes and success rate
        $questions = explode(Question::QUESTION_DELIMITER, $simpleqcm->questionids);
        $nbQuestions = count($questions);
        $nbAttempts = count($attempt->questions);
        $nbSuccesses = count(array_filter($attempt->questions, function ($question) {
            return $question->status == "success";
        }));
        $rate = $nbSuccesses/$nbQuestions;

        // Manage gradeBook
        $qcmGrade = $DB->get_record('simpleqcm_grades', array('userid' => $USER->id, 'simpleqcm' => $simpleqcm->id));
        if ($qcmGrade) {
            $newGrade = (float) $simpleqcm->grade * (float) $rate;
            if ($qcmGrade->grade < $newGrade) {
                $qcmGrade->grade = $newGrade;
                $DB->update_record('simpleqcm_grades', $qcmGrade);
            }
        }
        else {
            $qcmGrade = new stdClass();
            $qcmGrade->simpleqcm = $simpleqcm->id;
            $qcmGrade->userid = $USER->id;
            $qcmGrade->grade = $simpleqcm->grade * $rate;
            $qcmGrade->timemodified = time();
            $DB->insert_record("simpleqcm_grades", $qcmGrade);
        }
        simpleqcm_update_grades($simpleqcm, $USER->id);

        // Manage completion
        $completion = new completion_info($course);
        $complete = false;
        if(simpleqcm_get_completion_state($course,$cm,$USER->id)) {
            $completion->update_state($cm,COMPLETION_COMPLETE);
            $complete = true;
        } else {
            $completion->update_state($cm,COMPLETION_INCOMPLETE);
        }
        $completion->set_module_viewed($cm);

        //Update attempt info
        $currentAttempt->state = $nbAttempts == $nbQuestions ? "finish" : "inprogress";
        $currentAttempt->score = $simpleqcm->grade * $rate;
        $currentAttempt->sequence = json_encode($attempt);
        $currentAttempt->timemodified = time();
        if($nbAttempts == $nbQuestions){
            $currentAttempt->timefinish = time();
        }
        $DB->update_record('simpleqcm_attempts', $currentAttempt);

        echo json_encode(array(
            'status'        => 'success',
            'contents'      => $currentAttempt->state,
            'attempts'      => $nbAttempts,
            'rate'          => $rate,
            'current_grade' => $simpleqcm->grade * $rate,
            'best_grade'    => $qcmGrade->grade,
        ));
    }
}
return;
