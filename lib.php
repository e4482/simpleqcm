<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module simpleqcm
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the simpleqcm specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package     mod_simpleqcm
 * @author      Pascal Fautrero, Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('autoloader.php');
use mod_simpleqcm\local\models\SimpleQcmActivity;

defined('MOODLE_INTERNAL') || die();

define('MAX_QUESTION_NB', 25);
define('MAX_ANSWER_NB', 5);
global $CFG;
require_once($CFG->dirroot . '/lib/questionlib.php');


////////////////////////////////////////////////////////////////////////////////
// Moodle core API                                                            //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function simpleqcm_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:                 return true;
        case FEATURE_SHOW_DESCRIPTION:          return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:   return true;
        case FEATURE_COMPLETION_HAS_RULES:      return true;
        case FEATURE_GRADE_HAS_GRADE:           return true;
        case FEATURE_GRADE_OUTCOMES:            return true;
        case FEATURE_BACKUP_MOODLE2:            return true;
        default:                                return null;
    }
}

/**
 * Saves a new instance of the simpleqcm into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $simpleqcmform An object from the form in mod_form.php
 * @param mod_simpleqcm_mod_form $mform
 * @return int The id of the newly inserted simpleqcm record
 */
function simpleqcm_add_instance(stdClass $simpleqcmform, $mform) {
    global $DB;

    $simpleqcmform->timecreated = time();
    $simpleqcm = new SimpleQcmActivity($simpleqcmform);
    $simpleqcm->save();

    return $simpleqcm->id;
}

/**
 * Updates an instance of the simpleqcm in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $simpleqcm An object from the form in mod_form.php
 * @param mod_simpleqcm_mod_form $mform
 * @return boolean Success/Fail
 */
function simpleqcm_update_instance(stdClass $simpleqcmform, mod_simpleqcm_mod_form $mform = null) {

    $simpleqcmform->id = $simpleqcmform->instance;
    $simpleqcm = new SimpleQcmActivity($simpleqcmform);
    $simpleqcm->save();

    return $simpleqcm->id;
}

/**
 * Removes an instance of the simpleqcm from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function simpleqcm_delete_instance($id) {
    global $DB;

    if (! $simpleqcm = $DB->get_record('simpleqcm', array('id' => $id))) {
        return false;
    }

    # Delete any dependent records here #

    $DB->delete_records('simpleqcm', array('id' => $simpleqcm->id));
    $DB->delete_records('simpleqcm_grades', array('simpleqcm' => $simpleqcm->id));
    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return stdClass|null
 */
function simpleqcm_user_outline($course, $user, $mod, $simpleqcm) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $simpleqcm the module instance record
 * @return void, is supposed to echp directly
 */
function simpleqcm_user_complete($course, $user, $mod, $simpleqcm) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in simpleqcm activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function simpleqcm_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false
}

/**
 * Obtains the automatic completion state for this simpleqcm based on any conditions
 * in simpleqcm settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not, $type if conditions not set.
 */
function simpleqcm_get_completion_state($course,$cm,$userid,$type=COMPLETION_AND){
    global $DB;
    $simpleqcm = $DB->get_record('simpleqcm', array('id' => $cm->instance), '*', IGNORE_MISSING);
    if($simpleqcm){
        $grade = $DB->get_record('simpleqcm_grades', array('userid' => $userid, 'simpleqcm' => $simpleqcm->id), 'grade', IGNORE_MISSING);
        if($grade && $grade->grade >= $simpleqcm->grademin) return true;
    }
    return false;
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link simpleqcm_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function simpleqcm_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see simpleqcm_get_recent_mod_activity()}

 * @return void
 */
function simpleqcm_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function simpleqcm_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function simpleqcm_get_extra_capabilities() {
    return array();
}

////////////////////////////////////////////////////////////////////////////////
// Gradebook API                                                              //
////////////////////////////////////////////////////////////////////////////////

/**
 * Is a given scale used by the instance of simpleqcm?
 *
 * This function returns if a scale is being used by one simpleqcm
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $simpleqcmid ID of an instance of this module
 * @return bool true if the scale is used by the given simpleqcm instance
 */
function simpleqcm_scale_used($simpleqcmid, $scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('simpleqcm', array('id' => $simpleqcmid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of simpleqcm.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param $scaleid int
 * @return boolean true if the scale is used by any simpleqcm instance
 */
function simpleqcm_scale_used_anywhere($scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('simpleqcm', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}


/**
 * Delete grade item for given qcm
 *
 * @category grade
 * @param object $simpleqcm object
 * @return object simpleqcm
 */
function simpleqcm_grade_item_delete($simpleqcm) {
    global $CFG;
    require_once($CFG->libdir . '/gradelib.php');

    return grade_update('mod/simpleqcm', $simpleqcm->course, 'mod', 'simpleqcm', $simpleqcm->id, 0,
            null, array('deleted' => 1));
}



/**
 * Creates or updates grade item for the give simpleqcm instance
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass|SimpleQcmActivity $simpleqcm instance object with extra cmidnumber and modname property
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return void
 */
function simpleqcm_grade_item_update($simpleqcm, $grades=null) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    //error_log('grade item update simpleqcm');

    /** @example */
    $item = array();
    $item['itemname'] = clean_param($simpleqcm->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;
    $item['grademax']  = $simpleqcm->grade;
    $item['grademin']  = 0;

    if ($grades  === 'reset') {
        $item['reset'] = true;
        $grades = null;
    }
    // weird fix (from mod/quiz)
    if (!isset($simpleqcm->id)) $simpleqcm->id = $simpleqcm->instance;

    if (isset($simpleqcm->cmidnumber)) {
        //cmidnumber may not be always present
        $item['idnumber'] = $simpleqcm->cmidnumber;
    }

    return grade_update('mod/simpleqcm', $simpleqcm->course, 'mod', 'simpleqcm', $simpleqcm->id, 0, $grades, $item);
}

/**
 * Update simpleqcm grades in the gradebook
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $simpleqcm instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @return void
 */
function simpleqcm_update_grades(stdClass $simpleqcm, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    if ($grades = simpleqcm_get_user_grades($simpleqcm, $userid)) {
        simpleqcm_grade_item_update($simpleqcm, $grades);
    } else if ($userid) {
        $grade = new stdClass();
        $grade->userid   = $userid;
        $grade->rawgrade = NULL;
        $grade->timemodified = time();
        simpleqcm_grade_item_update($simpleqcm, $grade);

    } else {
        simpleqcm_grade_item_update($simpleqcm);
    }
}
/**
 * Return grade for given user or all users.
 *
 * @global object
 * @global object
 * @param object $forum
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function simpleqcm_get_user_grades($simpleqcm, $userid = 0) {
    global $CFG, $DB;

    $params = array($simpleqcm->id);
    $usertest = '';
    if ($userid) {
        $params[] = $userid;
        $usertest = 'AND u.id = ?';
    }
    return $DB->get_records_sql("
            SELECT
                u.id,
                u.id AS userid,
                qcmg.grade AS rawgrade,
                qcmg.timemodified AS dategraded
            FROM {user} u
            JOIN {simpleqcm_grades} qcmg ON u.id = qcmg.userid
            WHERE qcmg.simpleqcm = ?
            $usertest
            GROUP BY u.id, qcmg.grade, qcmg.timemodified", $params);
}
////////////////////////////////////////////////////////////////////////////////
// File API                                                                   //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function simpleqcm_get_file_areas($course, $cm, $context) {
    // "standalone_editor" is the file area used by all fields
    // question_$i and answer$j_$i
    // in the modedit.php view, standalone_editor is an editor element
    // All fields are edited in this only editor
    return array('standalone_editor');
}

/**
 * File browsing support for simpleqcm file areas
 *
 * @package mod_simpleqcm
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function simpleqcm_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the simpleqcm file areas
 *
 * @package mod_simpleqcm
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the simpleqcm's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function simpleqcm_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB;

    require_login($course, true, $cm);
    $entryid = (int)array_shift($args);
    $relativepath = implode('/', $args);
    if ($filearea == 'questiontext') {
        $component = 'question';
        $sql = 'SELECT cont.id FROM {context} AS cont
              INNER JOIN {question_categories} AS qc
              ON cont.id = qc.contextid
              INNER JOIN {question} AS q
              ON q.category = qc.id
              WHERE q.id = :qid';
        $filecontextid = $DB->get_record_sql($sql, ['qid' => $entryid], MUST_EXIST)->id;
    } else if ($filearea == 'answer') {
        $component = 'question';
        $sql = 'SELECT cont.id FROM {context} AS cont
              INNER JOIN {question_categories} AS qc
              ON cont.id = qc.contextid
              INNER JOIN {question} AS q
              ON q.category = qc.id
              INNER JOIN {question_answers} as ans
              ON ans.question = q.id
              WHERE ans.id = :ansid';
        $filecontextid = $DB->get_record_sql($sql, ['ansid' => $entryid], MUST_EXIST)->id;
    } else {
        // Not used for now as intro files are not directed there and there is no other text editor than intro and questions/answers
        $component = 'mod_simpleqcm';
        $filecontextid = context_module::instance($cm->id, MUST_EXIST)->id;
    }
    $fullpath = "/$filecontextid/$component/$filearea/$entryid/$relativepath";
    //$fullpath = "/$filecontext->id/mod_millionnaire/intro/1/bouton.png";
    //error_log($fullpath);
    $fs = get_file_storage();
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
}

////////////////////////////////////////////////////////////////////////////////
// Navigation API                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Extends the global navigation tree by adding simpleqcm nodes:
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the simpleqcm module instance
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 */
function simpleqcm_extend_navigation(navigation_node $simpleqcmnode, stdclass $course, stdclass $module, cm_info $cm) {
}

/**
 * Extends the settings navigation with the simpleqcm settings
 * - button to convert this module into a quiz module
 *
 * This function is called when the context for the page is a simpleqcm module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $simpleqcmnode {@link navigation_node}
 */
function simpleqcm_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $simpleqcmnode=null) {
    global $PAGE;
    $cmcontext = $PAGE->context;

    if ($cmcontext instanceof context_module) {
        $coursecontext = $cmcontext->get_course_context();
    }

    if ($coursecontext && has_capability('mod/quiz:addinstance', $coursecontext)) {
        $convertnode = navigation_node::create(
            get_string('converttoquiz', 'simpleqcm'),
            new moodle_url('/mod/simpleqcm/convert.php', ['cmid' => $cmcontext->instanceid, 'courseid' => $coursecontext->instanceid])
        );

        $beforekey = null;

        $simpleqcmnode->add_node($convertnode, $beforekey);
    }


}


