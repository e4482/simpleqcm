<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 14/10/19
 * Time: 09:49
 */
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once($CFG->dirroot . '/lib/questionlib.php');
require_once($CFG->dirroot . '/mod/simpleqcm/simpleqcmquestion_form.php');

// Input
$questionid = optional_param('questionid', null, PARAM_INT);
$contextid = required_param('contextid', PARAM_INT);
$courseid = required_param('courseid', PARAM_INT);

$url = new moodle_url('/mod/simpleqcm/question_edit.php');
$url->param('questionid', $questionid);
$url->param('contextid', $contextid);
$url->param('courseid', $courseid);

// Context and other set ups
$context = context::instance_by_id($contextid);
$PAGE->set_context($context);
$PAGE->set_url($url);

// Requirements :
// - be logged
require_login($courseid);
// - can edit course
$coursecontext = context_course::instance($courseid);
require_capability('moodle/course:update', $coursecontext);
// - question id belongs to activity : done when question is loaded
// => if context is course context, it's an add and there is no questionid
if ($coursecontext->id === $contextid) {
    if (!empty($questionid)) {
        throw new moodle_exception("Trying to access a question in a wrong context");
    }
} else {
    if ($context->contextlevel !== CONTEXT_MODULE) {
        throw new moodle_exception("Trying to access a question in a wrong context");
    }
    // Check course module belongs to course
    $coursemoduleid = $context->instanceid;
    global $DB;
    $coursemodulemodule = $DB->get_record('course_modules', ['id' => $coursemoduleid], 'course', MUST_EXIST)->course;
    if ($coursemodulemodule != $courseid) {
        throw new moodle_exception('Trying to get illegally access to a question');
    }
    require_capability('mod/quiz:manage', $context);
}

// Form
$mform = new simpleqcmquestion_form();
$toform = $mform->prepare_data($questionid, $contextid);
$toform->contextid = $contextid;
$toform->courseid = $courseid;

$mform->set_data($toform);

// Submitted form processing
if ($questiondata = $mform->get_data()) {
    $mform->set_data($questiondata);
}


// Even if some are useless, necessary to load page as a Moodle page
$PAGE->set_title('simpleqcmquestion');
$PAGE->set_heading('simpleqcmquestion');
$PAGE->requires->css('/mod/simpleqcm/css/questions.css');
$PAGE->requires->js('/mod/simpleqcm/js/questionForm2ModForm.js');


// output
echo $OUTPUT->header();// Unwanted elements hidden with specific CSS
echo $mform->display();
echo $OUTPUT->footer();
