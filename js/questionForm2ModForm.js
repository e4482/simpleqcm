const RIGHT_ANSWER_CLASS = 'simpleqcm_right_answer'
const WRONG_ANSWER_CLASS = 'simpleqcm_wrong_answer'
var questionBlockId = null
var hasChanged = false

/**
 * Send question form data to global module form data
 */
function communicateQuestionData(event) {
    if (event) {
        hasChanged = true
    }

    let question = new Object()
    question.questionBlockId = questionBlockId
    question.id = document.querySelector('input[name="questionid"]').value
    question.name = document.querySelector('input[name="name"]').value
    question.text = document.getElementById('id_questiontexteditor').value
    question.textFormat = document.querySelector("input[name='questiontexteditor[format]']").value
    question.textItemId = document.querySelector("input[name='questiontexteditor[itemid]']").value
    question.answers = []
    for (var i=0 ; i<5 ; i++) {
        var answereditor = 'answer' + i + 'editor'
        question.answers[i] = new Object()
        question.answers[i].id = document.querySelector("input[name='answer" + i + "id']").value
        question.answers[i].text = document.getElementById('id_' + answereditor).value
        question.answers[i].textFormat = document.querySelector("input[name='" + answereditor + "[format]']").value
        question.answers[i].textItemId = document.querySelector("input[name='" + answereditor + "[itemid]']").value
        question.answers[i].isCorrect = document.getElementById('id_answer' + i + 'iscorrect').checked
        let answerElement = document.getElementById('fgroup_id_answer' + i + 'group').querySelector('.felement div')
        if (question.answers[i].text == '' || question.answers[i].text == '<p></p>') {
            answerElement.classList.remove(RIGHT_ANSWER_CLASS)
            answerElement.classList.remove(WRONG_ANSWER_CLASS)
        } else {
            if (question.answers[i].isCorrect) {
                answerElement.classList.add(RIGHT_ANSWER_CLASS)
                answerElement.classList.remove(WRONG_ANSWER_CLASS)
            } else {
                answerElement.classList.add(WRONG_ANSWER_CLASS)
                answerElement.classList.remove(RIGHT_ANSWER_CLASS)
            }
        }
    }
    let message = new Object()
    message.object = 'updateQuestion'
    message.question = question
    window.parent.postMessage(message, M.cfg.wwwroot)
}

function askForData(questionBlockIdFromMessage) {
    questionBlockId = questionBlockIdFromMessage
    communicateQuestionData()
}

function validateQuestion() {
    if (hasChanged) {
        document.getElementById('id_submitbutton').click()
        hasChanged = false
    }
    let message = new Object()
    message.object = 'validationDone'
    message.questionBlockId = questionBlockId
    window.parent.postMessage(message, M.cfg.wwwroot)
}

function routeMessage(message) {
    if (message.origin != M.cfg.wwwroot) return

    switch (message.data.object) {
        case "validate":
            validateQuestion()
            break
        case "new index":
            askForData(message.data.questionBlockId)
            break
        default:
            console.log("unknown message, object " + message.data.object)
    }
}

// Add event listeners : input
document.querySelectorAll('input').forEach(
    (element) => {
        element.addEventListener('change', communicateQuestionData)
    })
document.querySelectorAll('textarea').forEach(
    (element) => {
        element.addEventListener('change', communicateQuestionData)
    })

//parent.addEventListener('click', validateQuestion)
window.addEventListener('message', routeMessage)