const STRING_DELIMITER = ';'

/* *********************************************************************************************************************
 *  Initialisation
 * ****************************************************************************************************************** */
const TEMPLATE_NAME = "QUESTION_INDEX"
const QUESTION_ID_IN_TEMPLATE = 'QUESTION_ID'
const TEMPLATE = document.getElementById('id_question' + TEMPLATE_NAME + '_header')
// Avoid loading iframe before it is correctly filled by storing the src
let TEMPLATE_I_FRAME = TEMPLATE.querySelector('iframe')
let TEMPLATE_SRC = TEMPLATE_I_FRAME.src
TEMPLATE_I_FRAME.src = ''
const NEW_QUESTION_NAME = "Nouvelle question"

let whereQuestionsShouldBeInsertedBefore = document.getElementById('id_modstandardelshdr')
let nbQuestionObject = document.querySelector('input[name="nbquestions"]')
var nextBlockQuestionID = 0
// Validation management
var pushedSubmitButton = null
var nbBlockToValidate = 0
var iFrameToLoad
// Error management
var errorByBlock = []
let submitButton1 = document.getElementById("id_submitbutton")
let submitButton2 = document.getElementById("id_submitbutton2")
const errorReducer = (accumulator, currentValue) => accumulator || currentValue

/* *********************************************************************************************************************
 *  Utilitary functions
 * ****************************************************************************************************************** */

/**
 * Get the DOM element of the current element
 * @param indexQuestion: index in form
 */
function getQuestion(indexQuestion) {
    return document.querySelector('fieldset[data-question-block-id="' + indexQuestion + '"]')
}

function getAllQuestions() {
    return document.querySelectorAll("fieldset[data-question-block-id]")
}

/**
 * Collapse the question for a better readness
 * @param indexQuestion: index in form
 */
function collapseQuestion(indexQuestion) {
    getQuestion(indexQuestion).classList.add('collapsed')
}

/**
 * Open question
 * @param indexQuestion: index in form
 */
function uncollapseQuestion(indexQuestion) {
    getQuestion(indexQuestion).classList.remove('collapsed')
}

function moveElementsToTheHeader(question) {
    // Move elements only when header is completed by Moodle javascripts
    let header = question.querySelector('legend')
    if (!header.querySelector('a')) return

    let shortText = question.querySelector('.mod-simpleqcm-displayed-question-on-header')
    let buttons = question.querySelector('fieldset')
    let nEA = question.querySelector('#error_not_enough_answers')
    let nRA = question.querySelector('#error_no_right_answer')

    // Elements may has already been moved
    if (shortText && shortText.parentElement !== header) {
        header.append(shortText)
        // Default value
        if (shortText.value == "") {
            shortText.value = NEW_QUESTION_NAME
        }
    }

    if (nEA && nEA.parentElement !== header) {
        header.append(nEA)
    }
    if (nRA && nRA.parentElement !== header) {
        header
            .append(nRA)
    }

    if (buttons && buttons.parentElement !== header) {
        let headerButtons = buttons.cloneNode(true)
        header.append(headerButtons)
        headerButtons.style.float = "right"
    }

}

function sendMessageToIFrame(iframe, message) {
    let iframeContent = iframe.contentWindow || iframe.contentDocument
    iframeContent.postMessage(message, M.cfg.wwwroot)
}

/**
 * Send to an iframe its identifier in order to manage reception of data
 * @param iframe
 */
function sendQuestionBlockId(iframe) {
    let message = new Object()
    message.object = "new index"
    message.questionBlockId = iframe.dataset.questionBlockId
    sendMessageToIFrame(iframe, message)
}

/**
 * Use to translate order of block to order of questions
 */
function updateOrder() {
    let allQuestions = getAllQuestions()
    nbQuestionObject.value = allQuestions.length
    let questionIsUnique = allQuestions.length == 1
    errorByBlock = []
    allQuestions.forEach((question, index) => {
        // Warn iFrame to get data
        let iframe = question.querySelector('iframe')
        iframe.dataset.questionBlockId = index
        sendQuestionBlockId(iframe)

        // Reset question number
        let displayedIndex = index + 1
        // in case this function is called before Moodle scripts, don't search in link
        var title = question.querySelector('legend a')
        if (!title) {
            title = question.querySelector('legend')
        }
        title.innerHTML = 'Question ' + displayedIndex

        // Move question name
        moveElementsToTheHeader(question)

        // Reset its data
        question.dataset.questionBlockId = index
        question.querySelectorAll('input[type="button"]').forEach(function (button) {
            button.dataset.questionBlockId = index
        })

        // Decide to display Remove Button
        if (questionIsUnique) {
            question.querySelector('input.mod-simpleqcm-question-delete').style.display = "none"
        } else {
            question.querySelector('input.mod-simpleqcm-question-delete').style.display = "inline-block"
        }

        // Error initialisation
        errorByBlock[index] = true
    })
}

function moveQuestion(indexQuestionToMove, indexQuestionInsertAfter) {

}


/**
 * Insert a question after the one containing the button,
 * move other questions if filled
 * @param questionId for iFrame if exists
 */
function addQuestion(questionId = null) {
    let previousBlockId
    if (event && event.target.dataset) {
        previousBlockId = event.target.dataset.questionBlockId
    } else {
        previousBlockId = null
    }

    // Create a new question from template.
    var newQuestion = TEMPLATE.cloneNode(true)
    let newQuestionIFrame = newQuestion.querySelector('iframe')
    if (questionId) {
        newQuestionIFrame.src = TEMPLATE_SRC.replace(QUESTION_ID_IN_TEMPLATE, questionId)
    } else {
        newQuestionIFrame.src = TEMPLATE_SRC
    }

    // Force set up but admit it is not filled yet (filled in updateOrder)
    newQuestion.dataset.questionBlockId = -1

    // Place it correctly
    let previousQuestion = getQuestion(previousBlockId)
    if (previousQuestion) {
        newQuestion = TEMPLATE.parentNode.insertBefore(newQuestion, previousQuestion.nextSibling)
        collapseQuestion(previousBlockId)
    } else {
        newQuestion = TEMPLATE.parentNode.insertBefore(newQuestion, whereQuestionsShouldBeInsertedBefore)
    }

    let newIndex = nextBlockQuestionID++

    // Change IDs
    var text = newQuestion.outerHTML
    var iTmpl = text.search(TEMPLATE_NAME)
    while (iTmpl >= 0) {
        text = text.replace(TEMPLATE_NAME, newIndex.toString())
        iTmpl = text.search(TEMPLATE_NAME)
    }
    newQuestion.outerHTML = text

    // Display it
    // As with last command, we lose connection to the new question, we force to get it with getQuestion(-1)
    newQuestion = getQuestion(-1)
    newQuestion.style.display = 'block'
    newQuestion.querySelector('input').focus()


    // Rename questions
    updateOrder()

}

/**
 * Empty a question area,
 * move other questions if filled
 * @param indexQuestion
 */
function removeQuestion() {
    // Remove it
    // TODO confirmation asked

    let indexQuestion = event.target.dataset.questionBlockId
    let question = getQuestion(indexQuestion)
    TEMPLATE.parentNode.removeChild(question)
    updateOrder()
}

/**
 * Call when iframe is loaded to resize and to send signal to load next iframe
 * @param iframe
 */
function frameLoaded(iframe) {
    // Don't do anything when template is loaded
    if (iframe.dataset.questionBlockId === undefined || iframe.dataset.questionBlockId === null) return

    // ** Resizing
    // Adding a margin in height so that script which makes appear editor stuffs doesn't make the height too small
    let heightMargin = 150 // in px
    let frameHeight = iframe.contentWindow.document.body.scrollHeight + heightMargin
    iframe.style.height = frameHeight + "px";

    // Send question block id as it wasn't ready yet
    updateOrder()

    // Load next I frame
    let nextIFrame = iFrameToLoad.shift()
    if (nextIFrame) {
        addQuestion(nextIFrame)
    }
}

function questionId (index) {return document.querySelector("input[name='question" + index + "id']")}
function questionText (index) {return document.querySelector("input[name='question" + index + "text']")}
function questionShortText (index) {return getQuestion(index).querySelector('.mod-simpleqcm-displayed-question-on-header')}
function questionName (index) {return  document.querySelector("input[name='question" + index + "name']")}
function questionTextFormat (index) {return document.querySelector("input[name='question" + index + "textformat']")}
function questionTextItemid (index) {return document.querySelector("input[name='question" + index + "textitemid']")}
function questionAnswerId (indexQ, indexA) {return document.querySelector("input[name='question" + indexQ + "_answer" + indexA + "id']")}
function questionAnswerText (indexQ, indexA) {return document.querySelector("input[name='question" + indexQ + "_answer" + indexA + "text']")}
function questionAnswerTextFormat (indexQ, indexA) {return document.querySelector("input[name='question" + indexQ + "_answer" + indexA + "textformat']")}
function questionAnswerTextItemid (indexQ, indexA) {return document.querySelector("input[name='question" + indexQ + "_answer" + indexA + "textitemid']")}
function questionAnswerIsCorrect (indexQ, indexA) {return document.querySelector("input[name='question" + indexQ + "_answer" + indexA + "iscorrect']")}

function errorMessageNotEnoughValidAnswers(question) {return question.querySelector("#error_not_enough_answers")}
function errorMessageNoRightAnswer(question) {return question.querySelector("#error_no_right_answer")}

function manageError() {
    if (errorByBlock.reduce(errorReducer)) {
        submitButton1.classList.add('mod-simpleqcm-invalidformbtn')
        submitButton1.type = 'button'
        submitButton1.removeEventListener('click', validateForm)
        if (submitButton2) {
            submitButton2.classList.add('mod-simpleqcm-invalidformbtn')
            submitButton2.type = 'button'
            submitButton2.removeEventListener('click', validateForm)
        }
    } else {
        submitButton1.classList.remove('mod-simpleqcm-invalidformbtn')
        submitButton1.type = 'submit'
        submitButton1.addEventListener('click', validateForm)
        if (submitButton2) {
            submitButton2.classList.remove('mod-simpleqcm-invalidformbtn')
            submitButton2.type = 'submit'
            submitButton2.addEventListener('click', validateForm)
        }

    }
}

function updateQuestion(question) {
    let questionBlockId = question.questionBlockId
    // Don't take into account uninitialized blocks
    if (!questionBlockId) return

    console.log("Mise à jour de la question " + questionBlockId)

    var nbRightAnswers = 0
    var nbValidAnswers = 0
    questionId(questionBlockId).value = question.id
    questionText(questionBlockId).value = question.text
    var shortText = question.text
    var loopNb = 0
    let regexpBalise = /<[^>\s][^>]*>/
    while (shortText.search(regexpBalise) >= 0 && loopNb < 1000) {
        shortText = shortText.replace(regexpBalise, "").replace()
        loopNb++
    }
    if (loopNb == 1000) {
        console.log("Erreur de recherche du pattern dans la simplification du texte de la question " + (questionBlockId+1))
    }
    questionName(questionBlockId).value = shortText
    let shortTextElement = questionShortText(questionBlockId)
    shortTextElement.innerHTML = " : " + shortText
    questionTextFormat(questionBlockId).value = question.textFormat
    questionTextItemid(questionBlockId).value = question.textItemId
    for (var i=0 ; i<5 ; i++) {
        if (question.answers[i].text != "" && question.answers[i].text != "<p></p>") {
            nbValidAnswers++
            questionAnswerText(questionBlockId, i).value = question.answers[i].text
            questionAnswerText(questionBlockId, i).value = question.answers[i].text
            questionAnswerTextFormat(questionBlockId, i).value = question.answers[i].textFormat
            questionAnswerTextItemid(questionBlockId, i).value = question.answers[i].textItemId
            questionAnswerId(questionBlockId, i).value = question.answers[i].id
            if (questionAnswerIsCorrect(questionBlockId, i).value = question.answers[i].isCorrect) {
                nbRightAnswers++
            }
        }
    }


    let questionBlock = getQuestion(questionBlockId)
    let errorMsgNVA = errorMessageNotEnoughValidAnswers(questionBlock)
    var blockIsInError = false
    if (nbValidAnswers < 2) {
        errorMsgNVA.style.display = ''
        shortTextElement.style.display = 'none'
        blockIsInError = true
    } else {
        errorMsgNVA.style.display = 'none'
        let errorMsgNRA = errorMessageNoRightAnswer(questionBlock)
        if (nbRightAnswers == 0) {
            errorMsgNRA.style.display = ''
            shortTextElement.style.display = 'none'
            blockIsInError = true
        } else {
            errorMsgNRA.style.display = 'none'
            shortTextElement.style.display = ''
        }
    }

    errorByBlock[questionBlockId] = blockIsInError
    manageError()
}

function warnIFramesToValidateForm() {
    let message = new Object()
    message.object = "validate"
    document.querySelectorAll('iframe').forEach( (iframe) => {
        let iframeContent = iframe.contentWindow || iframe.contentDocument
        iframeContent.postMessage(message, M.cfg.wwwroot)
    })
}

/**
 * Called when submit button is pressed.
 * Request all question blocks to validate in order to avoid useless alerts.
 * Real validation is done via a listener
 * @param event
 */
function validateForm(event) {
    if (pushedSubmitButton == null) {
        // Send request to all blocks to validate
        // Don't take the last as it is empty by construction
        nbBlockToValidate = parseInt(nbQuestionObject.value)
        pushedSubmitButton = event.srcElement || event.target
        event.preventDefault()
        warnIFramesToValidateForm()
    }
}

/**
 * Callback of validation message listener.
 * Will check if there is other blocks to wait to trigger the validation
 * @param questionBlockId
 */
function validateGlobalFormIfAllSubOnesAreValidated(questionBlockId) {

    // Remove block of the waited block list
    nbBlockToValidate--

    // Trigger validation if noone waits anymore
    if (nbBlockToValidate == 0) {
        pushedSubmitButton.click()
    }

}

function routeMessage(message) {
    if (message.origin != M.cfg.wwwroot) return

    switch (message.data.object) {
        case 'updateQuestion':
            updateQuestion(message.data.question)
            break
        case 'validationDone':
            validateGlobalFormIfAllSubOnesAreValidated(message.data.questionBlockId)
            break
        default:
            console.error('Not manage case: message ' + message.data.object)
    }
}

/* *********************************************************************************************************************
 *  Main instructions
 * ****************************************************************************************************************** */
// Hide template
TEMPLATE.style.display = 'none'

window.addEventListener("message", routeMessage, false)

// Adding question iframes
let questions = document.querySelector("input[name='questionids']").value.split(';')
iFrameToLoad = questions
if (iFrameToLoad.length > 0 && iFrameToLoad[iFrameToLoad.length-1] === "") {
    iFrameToLoad.pop()
}

if (iFrameToLoad.length > 0) {
    addQuestion(iFrameToLoad.shift())
} else {
    // Add first question if none exists
    addQuestion()
}