<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * simpleqcm stats
 *
 * @package   mod_simpleqcm
 * @category  grade
 * @copyright 2016 Pascal Fautrero
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once(dirname(__FILE__).'/locallib.php');
global $CFG;
$id = required_param('id', PARAM_INT);          // Course module ID
$cm         = get_coursemodule_from_id('simpleqcm', $id, 0, false, MUST_EXIST);
$course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
$simpleqcm  = $DB->get_record('simpleqcm', array('id' => $cm->instance), '*', MUST_EXIST);

$context        = context_course::instance($course->id);

//Among all attempts, we choose for each user, the one with the highest score and then the smallest timestamp
$qcm_attempts = $DB->get_records_sql('
SELECT d.userid, d.score, d.sequence, d.timefinish
FROM {simpleqcm_attempts} d
INNER JOIN (
  SELECT a.userid, MAX(a.timefinish) timefinish
  FROM {simpleqcm_attempts} a
  INNER JOIN (
    SELECT userid, MAX(score) score
    FROM {simpleqcm_attempts}
    WHERE simpleqcmid = ?
    GROUP BY userid
  ) b ON a.userid = b.userid AND a.score = b.score
  WHERE simpleqcmid = ?
  GROUP BY a.userid
) c ON d.userid = c.userid AND d.timefinish = c.timefinish
WHERE simpleqcmid = ?;
', [$simpleqcm->id,$simpleqcm->id,$simpleqcm->id]);

$answers = array();
for($i = 1; $i <= 20; $i++){
    $question = 'question_'.$i;
    if($simpleqcm->$question){
        $answers[$i] = [];
        for($j = 1; $j <= 5; $j++){
            $answer = 'answer'.$j.'_'.$i;
            if($simpleqcm->$answer) {
                $answers[$i][$j] = 0;
            }
        }
    }
}

$success = array();
$questions_label = array();
$total = array();
foreach($qcm_attempts as $qcm_attempt) {
  if ($qcm_attempt->sequence != "") {
    $seq = json_decode($qcm_attempt->sequence);
    foreach ($seq->questions as $key => $question) {
      if (!array_key_exists($key, $success)) $success[$key] = 0;
      if (!array_key_exists($key, $total)) $total[$key] = 0;
      if (!array_key_exists($key, $questions_label)) $questions_label[$key] = '"question ' . ($key + 1) . '"';
      if ($question->status =='success') $success[$key]++;
      for($i = 1; $i <= 5; $i++){
          if(isset($answers[$key+1][$i])){
              $answers[$key+1][$i] += $question->$i;
          }
      }
      $total[$key]++;
    }
  }
}

foreach ($success as $key => $value) {
  $success[$key] = number_format(($success[$key] / $total[$key] * 100), 0, ',', ' ');
}

$itemnumber = optional_param('itemnumber', 0, PARAM_INT);
$userid = optional_param('userid', 0, PARAM_INT);

$successStr= implode(",", $success);
$questions_label_str= implode(",", $questions_label);
$title = $simpleqcm->name;


$PAGE->set_context($context);
$PAGE->set_url('/mod/simpleqcm/grade.php', array('id' => $cm->id));
$PAGE->set_title(format_string($simpleqcm->name));
$PAGE->set_heading(format_string($course->fullname));
//$PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/simpleqcm/js/Chart.bundle.min.js')) ;
echo $OUTPUT->header();
?>
<header>
    <h1 style="display: inline">Statistiques : <?php echo $title ?></h1>
    <a href="/mod/simpleqcm/view.php?id=<?php echo $cm->id ?>"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Retour au QCM</a>
</header>
<canvas id='myChart' width='400' height='100' style='background-color:#ffffff;'></canvas>
<h2>Réponses données</h2>
<ul>
    <?php foreach($answers as $i => $answer) {
        $question = 'question_'.$i;
        echo '<li><strong>Question '.$i.' : '.$simpleqcm->$question.'</strong>';
        echo '<ul>';
        foreach($answer as $j => $a){
            $answer_id = 'answer'.$j.'_'.$i;
            $correct_id = 'correct'.$j.'_'.$i;
            echo '<li style="color:'.($simpleqcm->$correct_id ? 'green' : 'darkred').'">';
            $cm = get_coursemodule_from_instance('simpleqcm', $simpleqcm->id, $simpleqcm->course, false, MUST_EXIST);
            $context = context_module::instance($cm->id);
            $active_filters = getActiveFilters();
            $answer = applyFilters($simpleqcm->$answer_id, $active_filters, $context, "standalone_editor", 1);
            echo 'Réponse '.$j.' ('.$answer.') : '.$a.' participants</li>';
        }
        echo '</ul></li>';

    } ?>
</ul>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.2/Chart.min.js'></script>
<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [<?php echo $questions_label_str ?>],
        datasets: [{
            label: 'Taux de réussite %',
            data: [<?php echo $successStr ?>],
            backgroundColor: 'rgba(71, 163, 218, 1)'
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    max : 100,
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<?php
echo $OUTPUT->footer();
