<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use mod_simpleqcm\local\models\SimpleQcmActivity;
use vikimodule\Question;

/**
 * @package    mod_simpleqcm
 * @subpackage backup-moodle2
 * @copyright 2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_simpleqcm_activity_task
 */

/**
 * Structure step to restore one simpleqcm activity
 */
class restore_simpleqcm_activity_structure_step extends restore_questions_activity_structure_step {

    /**
     * @var SimpleQcmActivity
     */
    private $newsimpleqcm;

    protected function define_structure() {

        $paths = array();
        //$userinfo = $this->get_setting_value('userinfo');
        $paths[] = new restore_path_element('simpleqcm', '/activity/simpleqcm');

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_simpleqcm($simpleqcm) {
        global $DB;

        $simpleqcm = (object)$simpleqcm;
        if (empty($simpleqcm->questionids)) {
            // Old version case: upgrade
            $this->newsimpleqcm = SimpleQcmActivity::from_old_version($simpleqcm, $this->task->get_moduleid());
            $this->newsimpleqcm->upgrade(false);
            $simpleqcm->questionids = $this->newsimpleqcm->questionids;
        } else {
            // Newer version case : update question ids
            $oldquestionids = explode(Question::QUESTION_DELIMITER, $simpleqcm->questionids);
            $newquestionids = [];
            foreach ($oldquestionids as $oldquestionid) {
                $newquestionids[] = $this->get_mappingid('question', $oldquestionid);
            }
            $simpleqcm->questionids = implode(Question::QUESTION_DELIMITER, $newquestionids);
            // To be deleted
            for ($iQ = 1 ; $iQ <= 20 ; $iQ++) {
                $questionfieldname = "question_$iQ";
                $simpleqcm->$questionfieldname = '';
                for ($iA = 1 ; $iA <= 5 ; $iA++) {
                    $answerfieldname = "answer${iA}_${iQ}";
                    $simpleqcm->$answerfieldname = '';
                }
            }
        }

        $simpleqcm->course = $this->get_courseid();

        // insert the simpleqcm record
        $newitemid = $DB->insert_record('simpleqcm', $simpleqcm);
        $this->apply_activity_instance($newitemid);
    }

    protected function after_execute() {
        parent::after_execute();

        // Add simpleqcm related files
        $this->add_related_files('mod_simpleqcm', 'intro', null);
        // For compatibility
        $this->add_related_files('mod_simpleqcm', 'standalone_editor', null);
        // Upgrade if from an old module
        if  ($this->newsimpleqcm) {
            $this->newsimpleqcm->update_files();
        }
    }

    /**
     * When process_question_usage creates the new usage, it calls this method
     * to let the activity link to the new usage. For example, the quiz uses
     * this method to set quiz_attempts.uniqueid to the new usage id.
     * @param integer $newusageid
     */
    protected function inform_new_usage_id($newusageid)
    {
        // TODO: Implement inform_new_usage_id() method.
    }
}
