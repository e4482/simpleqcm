<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_data
 * @subpackage backup-moodle2
 * @copyright 2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_data_activity_task
 */

/**
 * Define the complete data structure for backup, with file and id annotations
 */
class backup_simpleqcm_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $simpleqcm = new backup_nested_element('simpleqcm', array('id'), array(
            'course', 'name', 'intro', 'introformat', 'timecreated', 'timemodified', 'grade',
            'grademin', 'questionids'));
        $coursecontext = new backup_nested_element('coursecontext', array('id'));
        $questioncat = new backup_nested_element('question_category', array('id'));
        $question = new backup_nested_element('question_in_category', array('id'));
        $answer = new backup_nested_element('answers', array('id'));

        // Build the tree.
        $simpleqcm->add_child($coursecontext);
        $coursecontext->add_child($questioncat);
        $questioncat->add_child($question);
        $question->add_child($answer);

        // Define sources.
        $simpleqcm->set_source_table('simpleqcm', array('id' => backup::VAR_ACTIVITYID));
        // As set_source_table doesn't accept non backup variables in sql request, prepare it now.
        $sql = "SELECT * FROM {context} WHERE contextlevel = " . CONTEXT_COURSE . " AND instanceid = :instanceid";
        $coursecontext->set_source_sql($sql, ['instanceid' => backup::VAR_COURSEID]);
        $questioncat->set_source_table('question_categories', ['contextid' => backup::VAR_PARENTID]);
        $question->set_source_table('question', ['category' => backup::VAR_PARENTID]);
        $answer->set_source_table('question_answers', ['question' => backup::VAR_PARENTID]);

        // Annotate files
        $simpleqcm->annotate_files('mod_simpleqcm', 'intro', null); // This file area hasn't itemid
        //$question->annotate_files('questions', 'questiontext', 'questionid') done after this during final task process
        //$question->annotate_files('mod_simpleqcm', 'questiontext', 'id');
        //$answer->annotate_files('mod_simpleqcm', 'answer', 'id');

        // Define id annotations.
        $question->annotate_ids('question', 'id');

        // Return the root element (data), wrapped into standard activity structure
        return $this->prepare_activity_structure($simpleqcm);
    }
}
