<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * French strings for simpleqcm
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_simpleqcm
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'QCM';
$string['modulenameplural'] = 'QCMs';
$string['modulename_help'] = '<h2>qcm</h2>'
	. "Cette activité vous permet de réaliser une série de questions à choix multiples.";
$string['simpleqcmfieldset'] = 'Custom example fieldset';
$string['simpleqcmname'] = 'QCM';
$string['simpleqcmname_help'] = 'This is the content of the help tooltip associated with the simpleqcmname field. Markdown syntax is supported.';
$string['simpleqcm'] = 'QCM';
$string['pluginadministration'] = 'Administration du QCM';
$string['pluginname'] = 'simpleqcm';
$string['name'] = 'Titre';
$string['grade'] = 'Nombre de points';
$string['grade_help'] = 'Nombre de points maximum alloués en cas de réussite.';
$string['grademin'] = 'Nombre de points minimal pour réussir';
$string['grademin_help'] = 'Nombre de points à obtenir pour que l\'activité soit considérée comme terminée.';
$string['questionlabel'] = 'Question';
$string['questiontext'] = 'Question';
$string['answer'] = 'Réponse';
$string['answeriscorrect'] = 'Réponse correcte ?';
$string['simpleqcm:addinstance'] = 'ajouter une activité QCM';
$string['simpleqcm:view'] = 'Voir une activité QCM';
$string['nosimpleqcms'] = 'Il n\'y a pas de QCM dans ce parcours';
$string['addquestion'] = 'Ajouter une question';
$string['deletequestion'] = 'Supprimer cette question';
$string['deletealluserdata'] = 'Supprimer toutes les tentatives des QCM';
$string['correctfeedback'] = 'Bonne réponse !';
$string['partiallycorrectfeedback'] = 'Mauvaise réponse...';
$string['incorrectfeedback'] = 'Mauvaise réponse...';
$string['converttoquiz'] = 'Convertir en activité Test';
$string['cantconvertcodeerror'] = 'Une erreur s\'est produite lors de la conversion du module, veuillez contacter l\'équipe de support';
$string['notenoughanswerserror'] = 'Les questions doivent comporter au moins deux réponses.';
$string['norightanswererror'] = 'Les questions doivent avoir au moins une bonne réponse.';
$string['noneoftheotheranswers'] = 'Aucune des autres réponses';
$string['incompleteactivitymessage'] = 'Activité en cours de conception';