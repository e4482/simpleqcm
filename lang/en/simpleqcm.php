<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for simpleqcm
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_simpleqcm
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'QCM';
$string['modulenameplural'] = 'QCMs';
$string['modulename_help'] = '<h2>qcm</h2>'
	. "This module allows you to create multi-choice questions sequence.";
$string['simpleqcmfieldset'] = 'Custom example fieldset';
$string['simpleqcmname'] = 'simpleqcm';
$string['simpleqcmname_help'] = 'This is the content of the help tooltip associated with the simpleqcmname field. Markdown syntax is supported.';
$string['simpleqcm'] = 'simpleqcm';
$string['pluginadministration'] = 'simpleqcm administration';
$string['pluginname'] = 'simpleqcm';
$string['name'] = 'Title';
$string['grade'] = 'number of points';
$string['grade_help'] = 'how many points are allocated when student succeeds.';
$string['grademin'] = 'Nombre de points minimal pour réussir';
$string['grademin_help'] = 'Nombre de points à obtenir pour que l\'activité soit considérée comme terminée.';
$string['questionlabel'] = 'Question';
$string['questiontext'] = 'Texte de la question';
$string['answer'] = 'Réponse';
$string['answeriscorrect'] = 'Réponse correcte ?';
$string['simpleqcm:addinstance'] = 'add simpleqcm module';
$string['simpleqcm:view'] = 'view simpleqcm module';
$string['nosimpleqcms'] = 'There is no QCM in the course';
$string['addquestion'] = 'Add a question';
$string['deletequestion'] = 'Delete this question';
$string['deletealluserdata'] = 'Delete all users tries';
$string['correctfeedback'] = 'OK';
$string['partiallycorrectfeedback'] = 'not totally OK';
$string['incorrectfeedback'] = 'KO';
$string['converttoquiz'] = 'Convert to quiz module';
$string['cantconvertcodeerror'] = 'Couldn\'t convert the module, please contact support';
$string['notenoughanswerserror'] = 'Questions must have at least two answers';
$string['norightanswererror'] = 'Question must have at least one right answer';
$string['noneoftheotheranswers'] = 'None of the others';
$string['incompleteactivitymessage'] = 'Activity in WIP';