<?php

namespace mod_simpleqcm\local\models;

use context_module;
use question_edit_contexts;
use stdClass;
use vikimodule\Activity;
use vikimodule\Editor;
use vikimodule\multichoice\MultichoiceAnswer;
use vikimodule\multichoice\MultichoiceQuestion;
use vikimodule\Question;
require_once($CFG->dirroot . '/mod/simpleqcm/lib.php');

defined('MOODLE_INTERNAL') || die();

class SimpleQcmActivity extends Activity
{
    /**
     * @var int minimal grade to succeed
     */
    public $grademin;

    /**
     * @var int id of the module Simpleqcm in the module table
     */
    private $moduleid;

    protected function moduleid(): int
    {
        if (empty($this->moduleid)) {
            global $DB;
            $this->moduleid = $DB->get_record('modules', ['name' => 'simpleqcm'], 'id', MUST_EXIST)->id;
        }
        return $this->moduleid;
    }

    protected function table(): string
    {
        return 'simpleqcm';
    }

    protected function component(): string
    {
        return 'mod_simpleqcm';
    }

    protected function old_file_areas_to_delete(): array
    {
        return ['standalone_editor'];
    }

    protected function create_from_form_specific(stdClass $form): void {
        if (!empty($form->coursemodule)) {
            $this->coursemodule = $form->coursemodule;
        } else {
            $this->update_course_module();
        }

        $this->grademin = $form->grademin;

        $context = context_module::instance($this->coursemodule, MUST_EXIST);
        $questioncontexts = new question_edit_contexts($context);
        $defaultcategory = question_make_default_categories(array($questioncontexts->lowest()));

        // Construct questions
        $this->questions = [];
        for ($blockindex = 0 ; $blockindex < MAX_QUESTION_NB ; $blockindex++) {
            $question = $this->process_question_block($form, $blockindex, $form->nbquestions, $defaultcategory->id);
            if (!empty($question)) {
                $this->questions[] = $question;
            }
        }
    }

    protected function load_from_db_specific(stdClass $indb): void
    {
        $this->questions = [];
        $questionids = explode(Question::QUESTION_DELIMITER, $this->questionids);

        // Incomplete activity case
        if (empty($questionids[0])) return;

        foreach ($questionids as $questionid) {
            $this->questions[] = new MultichoiceQuestion($questionid);
        }
    }

    private static function get_answer_in_form(stdClass $form, int $blockindex, int $answerindex): MultichoiceAnswer {
        $idlabel = self::answer_id_field($blockindex, $answerindex);
        $textlabel = self::answer_text_field($blockindex, $answerindex);
        $textformatlabel = self::answer_text_format_field($blockindex, $answerindex);
        $textitemidlabel = self::answer_text_item_id_field($blockindex, $answerindex);
        $iscorrectlabel = self::answer_is_correct_field($blockindex, $answerindex);

        return new MultichoiceAnswer(
            $form->$idlabel,
            new Editor(
                $form->$textlabel,
                $form->$textformatlabel,
                $form->$textitemidlabel
            ),
            $form->$iscorrectlabel
        );
    }

    private function process_question_block(stdClass $form, int $blockindex, int $blockindexmax, int $categoryid): ?MultichoiceQuestion {
        $blockindex = (int)$blockindex;
        if ($blockindex >= $blockindexmax) return null;

        // Process question database object
        $questionidlabel = self::question_id_field($blockindex);
        $questionnamelabel = self::question_name_field($blockindex);
        $questiontextlabel = self::question_text_field($blockindex);
        $questiontextformatlabel = self::question_text_format_field($blockindex);
        $questiontextitemidlabel = self::question_text_item_id_field($blockindex);

        // Process answers
        $answers = [];
        $nbcorrectanswers = 0;
        for ($iA = 0 ; $iA<MAX_ANSWER_NB ; $iA++) {
            $answer = $this->get_answer_in_form($form, $blockindex, $iA);
            if ($answer->is_valid()) {
                if ($answer->is_correct()) {
                    $nbcorrectanswers++;
                }
                $answers[] = $answer;
            } else {
                $answer->delete();
            }
        }

        // Not filled case
        if (empty($form->$questiontextlabel) && count($answers) == 0) {
            $questionid = $form->$questionidlabel;
            if (!empty($questionid)) {
                (new MultichoiceQuestion($questionid))->delete();
            }
            return null;
        }

        // This case may only happen when upgrading old activities
        if ($nbcorrectanswers == 0) {
            $answers[] = new MultichoiceAnswer(
                0,
                new Editor(get_string('noneoftheotheranswers', 'simpleqcm')),
                true
            );
        }

        $question = new MultichoiceQuestion(
            $form->$questionidlabel,
            $form->$questionnamelabel,
            new Editor(
                $form->$questiontextlabel,
                $form->$questiontextformatlabel,
                $form->$questiontextitemidlabel),
            $answers,
            $categoryid
        );

        $question->compute_answer_fraction();
        return $question;
    }

    /**
     * Save questions and compute questions field
     */
    protected function save_questions() {
        // Updating grades and questions references
        $questionids = [];
        foreach ($this->questions as $question) {
            $questionids[] = $question->save($this->coursemodule, 'simpleqcm');
        }

        // Completing form data for saving
        $this->questionids = implode(Question::QUESTION_DELIMITER, $questionids);
    }

    public function save(): void {
        global $DB;

        $this->save_questions();

        // To be deleted
        for ($iQ = 1 ; $iQ <= 20 ; $iQ++) {
            $questionfieldname = "question_$iQ";
            $this->$questionfieldname = '';
            for ($iA = 1 ; $iA <= 5 ; $iA++) {
                $answerfieldname = "answer${iA}_${iQ}";
                $this->$answerfieldname = '';
            }
        }

        // Create record if not already done
        if ($this->id == 0) {
            $this->id = $DB->insert_record('simpleqcm', $this, true);
        } else {
            $DB->update_record('simpleqcm', $this);
        }

        // useful for grade
        simpleqcm_grade_item_update($this);

    }

    public static function from_old_version(stdClass $oldmodule, $moduleid = null): Activity {
        $form = clone $oldmodule;
        // Upgrade of platform: keep id, restoring from an old version: creating new simpleqcm
        if (!empty($moduleid)) {
            unset($form->id);
        } else  {
            $form->instance = $oldmodule->id;
        }

        // Translation of question and answer fields
        for ($iQ = 0 ; $iQ < 20 ; $iQ++) {
            $questionidlabel = self::question_id_field($iQ);
            $questionnamelabel = self::question_name_field($iQ);
            $questiontextlabel = self::question_text_field($iQ);
            $questiontextformatlabel = self::question_text_format_field($iQ);
            $questiontextitemidlabel = self::question_text_item_id_field($iQ);

            $oldquestiontext = self::old_question_label($iQ);

            $form->$questionidlabel = 0;
            $form->$questionnamelabel = mb_substr(clean_param($form->$oldquestiontext, PARAM_TEXT), 0, 255);
            $form->$questiontextlabel = $form->$oldquestiontext;
            $form->$questiontextformatlabel = FORMAT_HTML;
            $form->$questiontextitemidlabel = null;    // Upgrade of file done later

            $nbCorrectAnswers = 0;
            for ($iA = 0 ; $iA < 5 ; $iA++) {
                $answeridlabel = self::answer_id_field($iQ, $iA);
                $answertextlabel = self::answer_text_field($iQ, $iA);
                $answertextformatlabel = self::answer_text_format_field($iQ, $iA);
                $answertextitemidlabel = self::answer_text_item_id_field($iQ, $iA);
                $answeriscorrectlabel = self::answer_is_correct_field($iQ, $iA);

                $oldanswertext = self::old_answer_label($iQ, $iA);
                $oldansweriscorrect = self::old_answer_is_correct($iQ, $iA);

                $form->$answeridlabel = 0;
                $form->$answertextlabel = $form->$oldanswertext;
                $form->$answertextformatlabel = FORMAT_HTML;
                $form->$answertextitemidlabel = null;
                $form->$answeriscorrectlabel = $form->$oldansweriscorrect;
            }
        }
        $form->nbquestions = 20; // max questions, real will be computed
        $form->coursemodule = $moduleid;

        return new SimpleQcmActivity($form);
    }

    public static function question_id_field(int $iQ): string {
        return "question${iQ}id";
    }

    public static function question_name_field(int $iQ): string {
        return "question${iQ}name";
    }

    public static function question_text_field(int $iQ): string {
        return "question${iQ}text";
    }

    public static function question_text_format_field(int $iQ): string {
        return "question${iQ}textformat";
    }

    public static function question_text_item_id_field(int $iQ): string {
        return "question${iQ}textitemid";
    }

    private static function answer_prefix(int $iQ, int $iA): string {
        return "question{$iQ}_answer{$iA}";
    }

    public static function answer_id_field(int $iQ, int $iA): string {
        return self::answer_prefix($iQ, $iA) . "id";
    }

    public static function answer_text_field(int $iQ, int $iA): string {
        return self::answer_prefix($iQ, $iA) . "text";
    }

    public static function answer_text_format_field(int $iQ, int $iA): string {
        return self::answer_prefix($iQ, $iA) . "textformat";
    }

    public static function answer_text_item_id_field(int $iQ, int $iA): string {
        return self::answer_prefix($iQ, $iA) . "textitemid";
    }
    public static function answer_is_correct_field(int $iQ, int $iA): string {
        return self::answer_prefix($iQ, $iA) . "iscorrect";
    }

    private static function old_answer_is_correct(int $iQ, int $iA) {
        $iQ++;
        $iA++;
        return "correct${iA}_${iQ}";
    }

    private static function old_answer_label(int $iQ, int $iA) {
        $iQ++;
        $iA++;
        return "answer{$iA}_${iQ}";
    }

    private static function old_question_label($iQ)
    {
        $iQ++;
        return "question_${iQ}";
    }

    public function check_questions(): bool
    {
        $complete = true;
        $i = 0;
        foreach ($this->get_questions() as $question) {
            $i++;
            $complete &= !$question->is_valid();
        }

        return !$complete;
    }
}