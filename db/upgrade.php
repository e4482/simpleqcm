<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the simpleqcm module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 *
 * @package    mod_simpleqcm
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_simpleqcm\local\models\SimpleQcmActivity;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/mod/simpleqcm/autoloader.php');

/**
 * Execute simpleqcm upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_simpleqcm_upgrade($oldversion) {
    global $DB, $CFG;

    $dbman = $DB->get_manager();

    if ($oldversion < 2016070700) {


        $qcm_table = new xmldb_table('simpleqcm_attempts');

        $qcmattemptid = new xmldb_field('id');
        $qcmattemptid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
        $qcm_table->addField($qcmattemptid);

        $qcmid = new xmldb_field('simpleqcmid');
        $qcmid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null);
        $qcm_table->addField($qcmid);

        $ownerid = new xmldb_field('userid');
        $ownerid->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $qcm_table->addField($ownerid);

        $state = new xmldb_field('state');
        $defaultvalue = 'inprogress';
        $state->set_attributes(XMLDB_TYPE_CHAR, '16', null, XMLDB_NOTNULL, null, $defaultvalue, null);
        $qcm_table->addField($state);

        $timestart = new xmldb_field('timestart');
        $timestart->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $qcm_table->addField($timestart);

        $timestart = new xmldb_field('timefinish');
        $timestart->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $qcm_table->addField($timestart);

        $timemodified = new xmldb_field('timemodified');
        $timemodified->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $qcm_table->addField($timemodified);

        $score = new xmldb_field('score');
        $score->set_attributes(XMLDB_TYPE_NUMBER, '10,5', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null , null);
        $qcm_table->addField($score);

        // primary key
        $key1 = new xmldb_key('primary');
        $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
        $qcm_table->addKey($key1);

        // foreign keys
        //$key2 = new xmldb_key('simpleqcmid_foreign');
        //$key2->set_attributes(XMLDB_KEY_FOREIGN, array('simpleqcmid'), 'simpleqcm', array('id'));
        //$qcm_table->addKey($key2);

        //$key3 = new xmldb_key('userid_foreign');
        //$key3->set_attributes(XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));
        //$qcm_table->addKey($key3);


        // indexes
        $qcm_table->add_index('userid_index', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $qcm_table->add_index('simpleqcmid_index', XMLDB_INDEX_NOTUNIQUE, array('simpleqcmid'));
        $qcm_table->add_index('state_index', XMLDB_INDEX_NOTUNIQUE, array('state'));

        // table creation
        $status = $dbman->create_table($qcm_table);

        upgrade_plugin_savepoint(true, 2016070700, 'mod', 'simpleqcm');
    }
    if ($oldversion < 2016070800) {


        $qcm_table = new xmldb_table('simpleqcm_attempts');

        $qcmsequence = new xmldb_field('sequence');
        $qcmsequence->set_attributes(XMLDB_TYPE_TEXT, 'small', null, XMLDB_NOTNULL, null, null, null);

        if (!$dbman->field_exists($qcm_table, $qcmsequence)) {
            $dbman->add_field($qcm_table, $qcmsequence);
        }

        upgrade_plugin_savepoint(true, 2016070800, 'mod', 'simpleqcm');
    }
    if ($oldversion < 2016070801) {


        $qcm_table = new xmldb_table('simpleqcm_attempts');

        $qcmcourse = new xmldb_field('courseid');
        $qcmcourse->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null , null);

        if (!$dbman->field_exists($qcm_table, $qcmcourse)) {
            $dbman->add_field($qcm_table, $qcmcourse);
        }

        upgrade_plugin_savepoint(true, 2016070801, 'mod', 'simpleqcm');
    }
    if ($oldversion < 2018053100) {

        $qcm_table = new xmldb_table('simpleqcm');

        $qcmgrademin = new xmldb_field('grademin');
        $qcmgrademin->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, 0 , null);

        if (!$dbman->field_exists($qcm_table, $qcmgrademin)) {
            $dbman->add_field($qcm_table, $qcmgrademin);
        }

        upgrade_plugin_savepoint(true, 2018053100, 'mod', 'simpleqcm');
    }

    if ($oldversion < 2020110500) {
        // Adding a temporary table for processing problematic activities later
        $errortable = new xmldb_table('simpleqcm_errors');

        if (!$dbman->table_exists($errortable)) {
            $field = new xmldb_field('id');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
            $errortable->addField($field);

            $field = new xmldb_field('simpleqcm');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'id');
            $errortable->addField($field);

            $field = new xmldb_field('coursemodule');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'simpleqcm');
            $errortable->addField($field);

            $field = new xmldb_field('questionids');
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null , 'coursemodule');
            $errortable->addField($field);

            // primary key
            $key1 = new xmldb_key('primary');
            $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
            $errortable->addKey($key1);

            $dbman->create_table($errortable);
        }

        // Adding questionids field
        $qcm_table = new xmldb_table('simpleqcm');
        $field = new xmldb_field('questionids');
        // max size = 20 * id max size = 20 * 10 = 200
        $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null , 'grademin');

        if (!$dbman->field_exists($qcm_table, $field)) {
            $dbman->add_field($qcm_table, $field);
        }

        // Process conversion before erasing data
        $oldmodules = $DB->get_records('simpleqcm', [], 'id', 'id');
        $nbmodules = count($oldmodules);
        $nextstep = $nbmodules / 10;
        $current = 0;
        mtrace("Upgrade of $nbmodules simple QCM modules\n");
        $oldfilterall = $CFG->filterall;
        $CFG->filterall = false;
        foreach ($oldmodules as $oldmodulewithid) {
            $oldmodule = $DB->get_record('simpleqcm', ['id' => $oldmodulewithid->id], '*', MUST_EXIST);
            if (++$current > $nextstep) {
                $progress = (int) $current / $nbmodules * 100;
                mtrace("\t $progress%\n");
                $nextstep = $current + $nbmodules / 10;
            }
            if (empty($oldmodule->questionids)) {
                $newmodule = SimpleQcmActivity::from_old_version($oldmodule);
                try {
                    $newmodule->upgrade(true);
                } catch (dml_exception $exception) {
                    $error = new stdClass();
                    $error->simpleqcm = $newmodule->id;
                    $error->questionids = $newmodule->questionids;
                    $simpleqcmmoduleid = $DB->get_record('modules', ['name' => 'simpleqcm'], 'id', MUST_EXIST)->id;
                    $error->coursemodule = $DB->get_record(
                        'course_modules', ['module' => $simpleqcmmoduleid, 'instance' => $newmodule->id],
                        'id', MUST_EXIST)->id;
                    $DB->insert_record('simpleqcm_errors', $error);
                }
                $newmodule->update_files();
            }
        }
        $CFG->filterall = $oldfilterall;
        mtrace("Upgrade completed\n");

        upgrade_plugin_savepoint(true, 2020110500, 'mod', 'simpleqcm');
    }

    if ($oldversion < 2020121100) {

        $backuptable = new xmldb_table('simpleqcm_backup');

        if (!$dbman->table_exists($backuptable)) {
            $field = new xmldb_field('id');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
            $backuptable->addField($field);

            $field = new xmldb_field('simpleqcm');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'id');
            $backuptable->addField($field);

            for ($iQ = 1 ; $iQ <= 20 ; $iQ++) {
                $field = new xmldb_field("question_$iQ");
                $field->set_attributes(XMLDB_TYPE_TEXT, 'big', true, null, null, null, null);
                $backuptable->addField($field);

                for ($iA = 1 ; $iA <= 5 ; $iA++) {
                    $field = new xmldb_field("answer{$iA}_{$iQ}");
                    $field->set_attributes(XMLDB_TYPE_TEXT, 'big', true, null, null, null, null);
                    $backuptable->addField($field);

                    $field = new xmldb_field("correct{$iA}_{$iQ}");
                    $field->set_attributes(XMLDB_TYPE_INTEGER, '1', true, null, null, null, null);
                    $backuptable->addField($field);
                }
            }

            // primary key
            $key1 = new xmldb_key('primary');
            $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
            $backuptable->addKey($key1);

            $dbman->create_table($backuptable);
        }

        // Backup
        mtrace("Backuping simple QCM modules");
        foreach ($DB->get_records('simpleqcm', [], '', 'id') as $modulewithid) {
            if ($DB->get_record('simpleqcm_backup', ['simpleqcm' => $modulewithid->id]) == false) {
                $module = $DB->get_record('simpleqcm', ['id' => $modulewithid->id], '*', MUST_EXIST);
                $module->simpleqcm = $module->id;
                unset($module->id);
                $DB->insert_record('simpleqcm_backup', $module);
            }
        }
        mtrace("... complete\n");

        $qcm_table = new xmldb_table('simpleqcm');

        // Retrieving activities that may have been corrupted because of iframe usage
        if ($dbman->field_exists('simpleqcm', 'question_1')) {
            $corruptedselectionarray = [];
            for ($iQ = 1 ; $iQ <= 20 ; $iQ++) {
                $corruptedselectionarray[] = "question_$iQ like '%<iframe%'";
                for ($iA = 1 ; $iA <= 5 ; $iA++) {
                    $corruptedselectionarray[] = "answer${iA}_$iQ like '%<iframe%'";
                }
            }

            $corrupted = $DB->get_records_select('simpleqcm', implode(" OR ", $corruptedselectionarray));
            mtrace("Found " . count ($corrupted) . " possibly corrupted simple QCM modules\n");
            foreach ($corrupted as $module) {
                $newmodule = SimpleQcmActivity::from_old_version($module);
                try {
                    mtrace("Re-upgrading module $newmodule->id");
                    $newmodule->upgrade(true);
                    mtrace("... completed\n");
                } catch (dml_exception $exception) {
                    $error = new stdClass();
                    $error->simpleqcm = $newmodule->id;
                    $error->questionids = $newmodule->questionids;
                    $simpleqcmmoduleid = $DB->get_record('modules', ['name' => 'simpleqcm'], 'id', MUST_EXIST)->id;
                    $error->coursemodule = $DB->get_record(
                        'course_modules', ['module' => $simpleqcmmoduleid, 'instance' => $newmodule->id],
                        'id', MUST_EXIST)->id;
                    $DB->insert_record('simpleqcm_errors', $error);
                    mtrace("... in error table\n");
                }
                $newmodule->update_files();
            }
        }

        // Deleting useless fields
        for ($iQ = 1 ; $iQ <= 20 ; $iQ++) {
            $field = new xmldb_field("question_$iQ");

            // Conditionally launch drop field grade.
            if ($dbman->field_exists($qcm_table, $field)) {
                $dbman->drop_field($qcm_table, $field);
            }

            for ($iA = 1 ; $iA <= 5 ; $iA++) {
                $field = new xmldb_field("answer{$iA}_{$iQ}");

                // Conditionally launch drop field grade.
                if ($dbman->field_exists($qcm_table, $field)) {
                    $dbman->drop_field($qcm_table, $field);
                }

                $field = new xmldb_field("correct{$iA}_{$iQ}");

                // Conditionally launch drop field grade.
                if ($dbman->field_exists($qcm_table, $field)) {
                    $dbman->drop_field($qcm_table, $field);
                }
            }
        }

        if ($dbman->table_exists('simpleqcm_errors')) {
            // Restablish questions for activities with errors
            $errorsdata = $DB->get_records('simpleqcm_errors');
            $nbmodules = count($errorsdata);
            $nextstep = $nbmodules / 10;
            $current = 0;
            mtrace("Restoring $nbmodules simple QCM modules\n");
            foreach ($errorsdata as $error) {
                if (++$current > $nextstep) {
                    $progress = (int) $current / $nbmodules * 100;
                    mtrace("\t $progress%\n");
                    $nextstep = $current + $nbmodules / 10;
                }
                $DB->set_field('simpleqcm', 'questionids', $error->questionids, ['id' => $error->simpleqcm]);
            }
            mtrace("Completed\n");
        }

        upgrade_plugin_savepoint(true, 2020121100, 'mod', 'simpleqcm');
    }
        // Final return of upgrade result (true, all went good) to Moodle.
    return true;
}
