<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* The main simpleqcm configuration form
*
* It uses the standard core Moodle formslib. For more info about them, please
* visit: http://docs.moodle.org/en/Development:lib/formslib.php
*
* @package    mod
* @subpackage simpleqcm
* @author     Nicolas Daugas <nicolas.daugas@ac-versailles.fr>
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot . '/lib/questionlib.php');
require_once($CFG->dirroot.'/mod/simpleqcm/lib.php');


/**
 * Module instance settings form
 */
class mod_simpleqcm_mod_form extends moodleform_mod {

    protected function definition() {
        global $PAGE;

        $mform = $this->_form;

        //-------------------------------------------------------------------------------
        // Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Name
        $mform->addElement('text', 'name', get_string('name', 'simpleqcm'), array('size'=>'64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');


        // Adding the standard "intro" and "introformat" fields
        $this->standard_intro_elements();

        // grades info
        $mform->addElement('text', 'grade', get_string('grade', 'simpleqcm'));
        $mform->setType('grade', PARAM_INT);
        $mform->setDefault('grade', 20);

        $mform->addElement('text', 'grademin', get_string('grademin', 'simpleqcm'));
        $mform->setType('grademin', PARAM_INT);
        $mform->setDefault('grademin', 0);


        // For order management
        $mform->addElement('hidden', 'questionids', '');
        $mform->setType('questionids', PARAM_TEXT);
        $mform->addElement('hidden', 'nbquestions', '');
        $mform->setType('nbquestions', PARAM_INT);
        $mform->setDefault('nbquestions', 0);

        // --------------------------------------------------------------------------------
        // Questions
        $this->create_question_block($this->context->id);

        // Create hidden fields to force posting validated data
        for ($iQ = 0; $iQ < MAX_QUESTION_NB ; $iQ++) {
            $mform->addElement('hidden', "question{$iQ}id");
            $mform->setType("question{$iQ}id", PARAM_INT);
            $mform->addElement('hidden', "question{$iQ}name");
            $mform->setType("question{$iQ}name", PARAM_TEXT);
            $mform->addElement('hidden', "question{$iQ}text");
            $mform->setType("question{$iQ}text", PARAM_RAW);
            $mform->addElement('hidden', "question{$iQ}textformat");
            $mform->setType("question{$iQ}textformat", PARAM_INT);
            $mform->setDefault("question{$iQ}textformat", FORMAT_HTML);
            $mform->addElement('hidden', "question{$iQ}textitemid");
            $mform->setType("question{$iQ}textitemid", PARAM_INT);

            for ($iA = 0 ; $iA < MAX_ANSWER_NB ; $iA++) {
                $mform->addElement('hidden', "question{$iQ}_answer{$iA}id");
                $mform->setType("question{$iQ}_answer{$iA}id", PARAM_INT);
                $mform->addElement('hidden', "question{$iQ}_answer{$iA}text");
                $mform->setType("question{$iQ}_answer{$iA}text", PARAM_RAW);
                $mform->addElement('hidden', "question{$iQ}_answer{$iA}textformat");
                $mform->setType("question{$iQ}_answer{$iA}textformat", PARAM_INT);
                $mform->setDefault("question{$iQ}_answer{$iA}textformat", FORMAT_HTML);
                $mform->addElement('hidden', "question{$iQ}_answer{$iA}textitemid");
                $mform->setType("question{$iQ}_answer{$iA}textitemid", PARAM_INT);
                $mform->addElement('hidden', "question{$iQ}_answer{$iA}iscorrect");
                $mform->setType("question{$iQ}_answer{$iA}iscorrect", PARAM_BOOL);
            }
        }

        // --------------------------------------------------------------------------------
        // Common module form
        $this->standard_coursemodule_elements();
        $this->add_action_buttons();

        $PAGE->requires->js('/mod/simpleqcm/js/addMoveRemoveQuestion.js');
        $PAGE->requires->css('/mod/simpleqcm/css/form.css');
    }

    private function create_question_block(int $contextid) {
        global $CFG;
        $mform = $this->_form;

        $questionblockid = 'QUESTION_INDEX';

        $mform->addElement('header', "question{$questionblockid}_header", get_string('questionlabel', 'simpleqcm') . $questionblockid);
        $mform->setExpanded("question{$questionblockid}_header");

        $mform->addElement('html', "<div class='mod-simpleqcm-displayed-question-on-header'></div>", '');

        $mform->addElement('html', "
<span id='error_not_enough_answers' class='label label-danger' style='display: none'>" .
            get_string('notenoughanswerserror', 'simpleqcm') . "
</span>
<span id='error_no_right_answer' class='label label-danger' style='display: none'>" .
            get_string('norightanswererror', 'simpleqcm') . "
</span>
        ");

        // QUESTION_INDEX != QUESTION_ID (index in form != index in DB)
        $params = "contextid=$contextid&courseid={$this->_course->id}&questionid=QUESTION_ID";

        $mform->addElement('html', "<iframe class='mod-simpleqcm-questionframe' id='question{$questionblockid}-frame' 
            onload='frameLoaded(this)'
            src='{$CFG->wwwroot}/mod/simpleqcm/question_edit.php?$params'></iframe>");

        $buttons = [];
        $buttons[] = $mform->createElement('button', "add_question_${questionblockid}", get_string('addquestion', 'simpleqcm'), [
            'onclick' => "addQuestion()",
            'class' => 'mod-simpleqcm-question-add'
        ]);
        $buttons[] = $mform->createElement('button', "delete_question_${questionblockid}", get_string('deletequestion', 'simpleqcm'), [
            'onclick' => "removeQuestion()",
            'class' => 'mod-simpleqcm-question-delete'
        ]);
        $mform->addGroup($buttons, "question{$questionblockid}_buttons", '', array(' '), false);
        $mform->setType("question{$questionblockid}_buttons", PARAM_RAW);

    }
}
