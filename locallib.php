<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Internal library of functions for module simpleqcm
 *
 * All the simpleqcm specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_simpleqcm
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_simpleqcm\local\models\SimpleQcmActivity;
use vikimodule\Question;

defined('MOODLE_INTERNAL') || die();

/**
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 */
//function simpleqcm_do_something_useful(array $things) {
//    return new stdClass();
//}



class simpleqcmNavigationModule {

    protected $courseid;
    protected $current_module_type;
    protected $current_module_id;
    protected $current_course_module;
    protected $section_parent;

    /***************************************************
     *
     * return @array
     *
     * *************************************************/
    public function __construct($courseid, $moduletype, $moduleid) {

        $this->courseid = $courseid;
        $this->current_module_type = $moduletype;
        $this->current_module_id = $moduleid;

    }
    /***************************************************
     *
     * return @array
     *
     * *************************************************/
    public function get_modules_list() {
        global $DB;
        $course_sections = $DB->get_records_sql("
            SELECT id, sequence
            FROM {course_sections}
            WHERE course = '" . $this->courseid . "'
            ORDER BY section ASC");
        $global_sequence = "";
        $final_array = [];
        foreach($course_sections as $course_section) {
            $global_sequence .= $course_section->sequence . ",";
        }

        $course = $DB->get_record('course',array('id' => $this->courseid));
        $modinfo = get_fast_modinfo($course);
        $modules_array = explode(",", $global_sequence);
        foreach ($modules_array as $cmid) {
          if ($cmid) {
            $cm = $modinfo->get_cm($cmid);
            if (($cm->visible) && ($cm->uservisible)) {
              array_push($final_array, $cmid);
            }
          }
        }
        // get the next module
        $this->current_course_module = $DB->get_record_sql("
            SELECT cm.id
            FROM {course_modules} as cm, {modules} as m
            WHERE cm.course = '" . $this->courseid . "'
            AND cm.module = m.id
            AND m.name = '" . $this->current_module_type . "'
            AND cm.instance = " . $this->current_module_id . "
        ");

        return $final_array;

    }
    public function get_parent_section($course_module_id) {

        global $DB;
        $parent_section = null;
        $course_sections = $DB->get_records_sql("
            SELECT id, sequence, section
            FROM {course_sections}
            WHERE course = '" . $this->courseid . "'
            ORDER BY section ASC");
        $global_sequence = "";
        foreach($course_sections as $course_section) {
            $sequence_array = explode(",", $course_section->sequence);
            if (in_array($course_module_id, $sequence_array)) {
                $parent_section = $course_section->section;
            }
        }
        return $parent_section;

    }
    /****************************************************
     *
     *
     *
     * **************************************************/

    public function html() {

        global $DB;

        $global_sequence_array = $this->get_modules_list();
        $nextModule = "";
        $key = array_search($this->current_module_id, $global_sequence_array);

        if ($key !== false) {
            if (array_key_exists($key + 1, $global_sequence_array) &&
                $global_sequence_array[$key + 1] != '') {
                $next_course_module = $DB->get_record_sql("
                    SELECT cm.instance, m.name
                    FROM {course_modules} as cm, {modules} as m
                    WHERE cm.id = " . $global_sequence_array[$key + 1] . "
                    AND m.id = cm.module
                ");
                if ($next_course_module->name != 'label') {
                    $nextModule = "<a href='/mod/"
                        . $next_course_module->name
                        . "/view.php?id="
                        . $global_sequence_array[$key + 1]
                        . "'><img src='/mod/simpleqcm/pix/next.png' title='étape suivante' alt='next' style='margin:5px;' /></a>";
                }
                else {
                    $nextModule = "<a href='/course/"
                        . "view.php?id="
                        . $this->courseid
                        . "&section="
                        . $this->get_parent_section($global_sequence_array[$key + 1])
                        . "'><img src='/mod/simpleqcm/pix/next.png' title='étape suivante' style='margin:5px;' alt='next' /></a>";
                }
            }

        }
        return $nextModule;
    }
}


function simpleqcm_create_game(SimpleQcmActivity $simpleqcm, $map, $qcmid){
    global $USER, $COURSE, $DB;

    if (!$simpleqcm->check_questions()) {
        return "<div class='notification'>" . get_string('incompleteactivitymessage', 'simpleqcm') . "</div>";
    }

    $cm = get_coursemodule_from_instance('simpleqcm', $simpleqcm->id, $simpleqcm->course, false, MUST_EXIST);
    $context = context_module::instance($cm->id);

    $desc = "";
    $title = $simpleqcm->name;
    if ($simpleqcm->intro != "") {
      $desc = $simpleqcm->intro_editor($context->id)->get_filtered_content($context, 'mod_simpleqcm');
      $hideheader = "";
    }
    else {
      $hideheader = "hideheader";
    }
    $module_type = 'simpleqcm';
    $module_id = $cm->id;
    $navigationModuleButton = new simpleqcmNavigationModule($COURSE->id, $module_type, $module_id);
    $nextModule = $navigationModuleButton->html();
    $gameid = "game" . rand(100,999);
    $gameid_lost = $gameid . "_lost";
    $gameid_reload = $gameid . "_reload";
    $gameid_won = $gameid . "_won";
    $gameid_final = $gameid . "_final";

    $questions = $simpleqcm->get_questions();

    $questionsForJson = [];

    foreach ($questions as $question){

        $answers = $question->get_answers();
        $answersForJson = [];
        foreach ($answers as $index => $answer) {
            $answereditor = $answer->get_answer();
            $answereditor->reduce_size();
            $answersForJson[] = array(
                'text' => $answereditor->get_filtered_content($context, 'mod_simpleqcm'),
                'correct' => $answer->is_correct(),
                'id'	 => $answer->get_id()
            );
        }

        $questionsForJson[] = array(
            'text' => $question->get_question()->get_filtered_content($context, 'mod_simpleqcm'),
            'weight' => '300',
            'answers' => $answersForJson
        );
    }

    $questions_json = json_encode($questionsForJson, JSON_PRETTY_PRINT);

    $maplogo = "";
    $mapreturn = "";
    if ($map) {
        $mapreturn = "<a href='/mod/mapmodules/view.php?id=$map'><img src='/mod/simpleqcm/pix/map.png' alt='Retourner à la map' title='Retourner à la map'></a>";
    }

    $mapreload = "<a href='/mod/simpleqcm/view.php?id=$qcmid&map=$map' id='$gameid_reload' ><img src='/mod/simpleqcm/pix/redo.png' alt='recharger ?' title='relancer ?'></a>";

    $game = <<<EOT

    <header class="simpleqcmtitle $hideheader">
        <h2>$title</h2>
        <h4>$desc</h4>
        <div class="corner"></div>
    </header>

    <div class="game_box">
        <div id='$gameid' class='game'>
            <div class="lost" id="$gameid_lost">
                <div class="sublost">
                    <div class="sublostbox">Mauvaise réponse...</div>
                </div>
            </div>
            <div class="won" id="$gameid_won">
                <div class="subwon">
                    <div class="subwonbox">Bonne réponse !</div>
                </div>
            </div>
            <div class="final" id="$gameid_final"></div>
    </div>

    </div>
    <script>
        //console.log("initialize game");

        var nbGoodAnswers = 0;
        var nbBadAnswers = 0;
        var expectedGoodAnswers = 0;
        var finalScore = 0;
        var expectedScore = 0;
        // Object to record user activity
        var questionObject = function(){
            this.status = 'inprogress'
        }
        var AttemptObject = function (){
            this.questions = new Array()
            this.currentindex = 0
        }
        AttemptObject.prototype.newQuestion = function() {
            var newquestion = new questionObject()
            this.questions.push(newquestion)
            this.currentindex = this.questions.length -1
            return this.currentindex
        }
        AttemptObject.prototype.getQuestion = function(index) {
            return this.questions[index]
        }
        AttemptObject.prototype.setAnswer = function(qindex, aindex, value) {
            this.questions[qindex][aindex] = value
        }
        AttemptObject.prototype.setQuestionStatus = function(qindex, value) {
            this.questions[qindex].status = value
        }
                // Managing classes
        var hasClass = function(ele,cls) {
            return (' ' + ele.className + ' ').indexOf(' ' + cls + ' ') > -1;
        }

        var addClass = function(ele,cls) {
          if (!hasClass(ele,cls)) ele.className += " "+cls;
        }
        var removeClass = function(ele,cls) {
          if (hasClass(ele,cls)) {
            var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
            ele.className=ele.className.replace(cls,' ');
          }
        }
        var verify_func = function(el, attempt) {

          var xhReq = new XMLHttpRequest();
          expectedGoodAnswers = el.getAttribute("data-good")
          if ((nbGoodAnswers == expectedGoodAnswers) && (nbBadAnswers == 0)) {
            attempt.setQuestionStatus(attempt.currentindex, 'success')
            var won = document.getElementById(gameid + "_won");
            won.setAttribute("style", "display:table;height:" + game.offsetHeight + "px");
            finalScore++;
            // send XMLHTTPRequest to mod_simpleqcm to record the success
            var rate = finalScore / expectedScore;
            /*xhReq.open("GET", "/mod/simpleqcm/validate.ajax.php?rate=" + rate + "&qcmid=$simpleqcm->id&sesskey=$USER->sesskey", false);
            xhReq.send(null);*/
          }
          else {
            attempt.setQuestionStatus(attempt.currentindex, 'fail')
            var lost = document.getElementById(gameid + "_lost");
            lost.setAttribute("style", "display:table;height:" + game.offsetHeight + "px");
          }
          xhReq.open("POST", "/mod/simpleqcm/attempt.ajax.php", false);
          xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
          xhReq.send("attempt=" + JSON.stringify(attempt) + "&qcmid=$simpleqcm->id&sesskey=$USER->sesskey");

          setTimeout(function(){
              var next = el.getAttribute("data-next");
              if (won) won.setAttribute("style", "display:none;");
              if (lost) lost.setAttribute("style", "display:none;");
              if (next != "end") {
                nbGoodAnswers = 0;
                nbBadAnswers = 0;
                document.getElementById(next).setAttribute('style', 'display:block');
                el.parentNode.setAttribute('style', 'display:none');
                qindex = attempt.newQuestion()
              }
              else {
                xhReq.open("GET", "/mod/simpleqcm/getnextmodule.ajax.php?qcmid=$simpleqcm->id&sesskey=$USER->sesskey", false);
                xhReq.onreadystatechange = function () {
                  if(xhReq.readyState === XMLHttpRequest.DONE && xhReq.status === 200) {
                      var response = JSON.parse(xhReq.response)
                      var final = document.getElementById(gameid + "_final");
                      final.setAttribute("style", "display:table;height:" + game.offsetHeight + "px");
                      var subFinal = document.createElement("div");
                      subFinal.setAttribute("class", "subfinal");
                      final.appendChild(subFinal);
                      var subFinalBox = document.createElement("div");
                      subFinalBox.setAttribute("class", "subfinalbox");
                      subFinal.appendChild(subFinalBox);
                      var text = document.createElement("p");
                      text.innerHTML = "Activité terminée";
                      subFinalBox.appendChild(text);
                      for (var i = 0;i < finalScore;i++) {
                        var starYellow = document.createElement("img");
                        starYellow.setAttribute("style", "max-width:"+ (subFinalBox.offsetWidth/expectedScore) + "px");
                        starYellow.setAttribute("src", "/mod/simpleqcm/pix/star_yellow.png");
                        subFinalBox.appendChild(starYellow);
                      };
                      for (var i = 0;i < expectedScore - finalScore;i++) {
                        var starGrey = document.createElement("img");
                        starGrey.setAttribute("style", "max-width:"+ (subFinalBox.offsetWidth/expectedScore) + "px");
                        starGrey.setAttribute("src", "/mod/simpleqcm/pix/star_grey.png");
                        subFinalBox.appendChild(starGrey);
                      };
                      var buttons = document.createElement("div");
                      buttons.setAttribute("class", "simpleqcm_buttons");
                      buttons.innerHTML = "$mapreload" + "$mapreturn" + response.contents
                      subFinalBox.appendChild(buttons);

                  }
                };
                xhReq.send(null);
                console.log(finalScore + "/" + expectedScore);
              }
          }, 2000);
        };
        var good_func = function(el, attempt) {

            if (!hasClass(el, "frage_answer_select")) {
                nbGoodAnswers++;
                removeClass(el, "frage_answer");
                addClass(el, "frage_answer_select");
                attempt.setAnswer(attempt.currentindex, el.getAttribute('data-id'), 1)
            }
            else {
                nbGoodAnswers--;
                removeClass(el, "frage_answer_select");
                addClass(el, "frage_answer");
                attempt.setAnswer(attempt.currentindex, el.getAttribute('data-id'), 0)
            }
        };
        var bad_func = function(el, attempt) {
            if (!hasClass(el, "frage_answer_select")) {
                nbBadAnswers++;
                removeClass(el, "frage_answer");
                addClass(el, "frage_answer_select");
                attempt.setAnswer(attempt.currentindex, el.getAttribute('data-id'), 1)
            }
            else {
                nbBadAnswers--;
                removeClass(el, "frage_answer_select");
                addClass(el, "frage_answer");
                attempt.setAnswer(attempt.currentindex, el.getAttribute('data-id'), 0)
            }
            console.log("nbBadAnswers " + nbBadAnswers);
        };
        var shuffleArray = function(tab) {
            for (var i = tab.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = tab[i];
                tab[i] = tab[j];
                tab[j] = temp;
            }
            return tab;
        }
        var init = function(questions, game, attempt) {
            nbGoodAnswers = 0;
            nbBadAnswers = 0;
            finalScore = 0;
            qindex = attempt.newQuestion()
            expectedScore = questions.length;
            var xhReq = new XMLHttpRequest();
            xhReq.open("POST", "/mod/simpleqcm/attempt.ajax.php", false);
            xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            xhReq.send("attempt=null&qcmid=$simpleqcm->id&sesskey=$USER->sesskey");
            for (var k=0;k < questions.length;k++) {
                expectedGoodAnswers = 0;
                questions[k].answers = shuffleArray(questions[k].answers);
                if (document.getElementById(gameid + "_verify" + k) != null) {
                    var q1 = document.getElementById(gameid + "_verify" + k);
                    q1.parentNode.removeChild(q1);
                }
                if (document.getElementById(gameid + "_q" + k) == null) {
                    var q1 = document.createElement("div");
                    q1.setAttribute("id", gameid + "_q" + k);
                    game.appendChild(q1);
                }
                else {
                    var q1 = document.getElementById(gameid + "_q" + k);
                    while (q1.firstChild) {
                        q1.removeChild(q1.firstChild);
                    }
                }
                if (k != 0) {
                    q1.setAttribute("style", "display:none");
                }
                else {
                    q1.setAttribute("style", "display:block");
                }
                var text = document.createElement("div");
                text.setAttribute("class", "frage");
                text.innerHTML = "<div class='frageInner'>"+questions[k].text+"</div>";
                q1.appendChild(text);
                var prefixes = ["A ", "B ", "C ", "D ", 'E '];
                for (var i=0;i < questions[k].answers.length;i++) {
                    var answer = document.createElement("div");
                    answer.setAttribute("class", "frage frage_answer");
                    answer.innerHTML = "<div class='frageInner'>" +
                        "<span style='color:black;'>" + prefixes[i] +
                        "</span>" +
                        questions[k].answers[i].text +
                        "</div>";
                    if (questions[k].answers[i].correct) {
                        expectedGoodAnswers++;
                        answer.addEventListener("click", function(){good_func(this, attempt)}, false);
                        answer.setAttribute("id", gameid + "_good" + k);

                    }
                    else {
                        answer.addEventListener("click", function(){bad_func(this, attempt)}, false);
                        answer.setAttribute("data-good", gameid + "_good" + k);
                    }
                    answer.setAttribute("data-id", questions[k].answers[i].id);
                    q1.appendChild(answer);
                }
                var verify = document.createElement("div");
                verify.setAttribute("id", gameid + "_verify" + k);
                verify.setAttribute("class", "frage frage_verify");
                verify.setAttribute("data-good", expectedGoodAnswers);
                verify.setAttribute("data-questionid", k);
                verify.innerHTML = "Vérifier la réponse";
                verify.addEventListener("click", function(){
                  verify_func(this, attempt)
                }, false);
                if (k != questions.length -1) {
                    verify.setAttribute("data-next", gameid + "_q" + (k+1));
                }
                else {
                    verify.setAttribute("data-next", "end");
                }
                q1.appendChild(verify);
            }
        };
        /*
         *
         *	GAME CREATION
         *
         */
        var gameid = "$gameid";
        var game = document.getElementById(gameid);
        var questions = $questions_json;
        var attempt = new AttemptObject()
        init(questions, game, attempt);
    </script>
    <style>
        @font-face {
            font-family: 'fine_lineregular';
            src: url('/mod/simpleqcm/font/fine_line-webfont.eot');
            src: url('/mod/simpleqcm/font/fine_line-webfont.eot?#iefix') format('embedded-opentype'),
                 url('/mod/simpleqcm/font/fine_line-webfont.woff2') format('woff2'),
                 url('/mod/simpleqcm/font/fine_line-webfont.woff') format('woff'),
                 url('/mod/simpleqcm/font/fine_line-webfont.svg#fine_lineregular') format('svg');
            font-weight: normal;
            font-style: normal;

        }
        .simpleqcm_buttons {
            margin:10px;
        }
        .mod-indent-outer {
            display: block;
        }
        .game_box {
            position:relative;
            width:100%;
        }
        .game {
            top:0px;
            left:0px;
            bottom:0px;
            right:0px;
            display:block;
            border:0px solid black;
            margin:0px auto;
            padding-top:10px;
            background: #e5e5e5;
        }
        .frage {
            width: 84%;
            margin:20px auto;
            padding: 5px;
            text-align: center;
            position: relative;
            display: table;
            font-size: 1.25em;
            text-align: center;
            vertical-align: middle;
            color: #000;
            font-weight: bold;
            font-family: Arial;
        }
        .lost, .won, .final{
            z-index:1000;
            display: none;
            position:absolute;
            text-align:center;
            width:100%;
            height:510px;
            top:0;
            right:0;
            left:0;
            bottom:0;
            background: rgba(0, 0, 0, 0.8);
            z-index:100;
        }
        .sublost, .subwon, .subfinal{
            display:table-cell;
            vertical-align: middle;
        }
        .sublostbox, .subwonbox, .subfinalbox{
           /* font-family: fine_lineregular, yanone_thin, Arial;*/
            font-size: 1.5em;
            color:white;
            border-radius:10px;
            padding:30px;
            margin:0px auto;
            width:50%;
            border: 5px solid #145072;
            background: linear-gradient(#5F9CC5, #396B9E) repeat scroll 0% 0% #2567AB;
            font-weight: bold;
            text-shadow: 0px -1px 1px #145072;
        }

        .frage_answer{
            margin:5px auto;
            width:70%;
            height:auto;
            border: 0px solid #145072;
            /*background: linear-gradient(#5F9CC5, #396B9E) repeat scroll 0% 0% #2567AB;*/
            background-color: #47A3DA;
            font-weight: bold;
            color: #FFF;
            /*text-shadow: 0px -1px 1px #145072;*/
            text-align:left;
            padding-left:10px;
        }
        .frage_answer_select{
            margin:5px auto;
            width:70%;
            height:auto;
            border: 0px solid #145072;
            font-weight: bold;
            color: #FFF;
            text-align:left;
            padding-left:10px;
            background-color: #90A42E;
        }
        .frage_answer_select:hover{
            cursor:pointer;
        }
        .frage_verify{
            margin:20px auto;
            width:30%;
            height:auto;
            border-radius:5px;
            border: 0px solid #145072;
            /*background: linear-gradient(#5F9CC5, #396B9E) repeat scroll 0% 0% #2567AB;*/
            background-color: #47A3DA;
            font-weight: bold;
            color: #FFF;
            /*text-shadow: 0px -1px 1px #145072;*/
            text-align:center;
            padding:20px;
            font-size:1.8em;
        }
        .frage_answer:hover{
            background: none repeat scroll 0% 0% rgba(120, 100, 250, 0.9);
            cursor:pointer;
        }
        .frage_verify:hover{
            background: none repeat scroll 0% 0% rgba(120, 100, 250, 0.9);
            cursor:pointer;
        }
        .frageInner {
            display: table-cell;
            width: 100%;
            height: 100%;
        }
        .simpleqcmtitle {
            position:relative;
            text-align:center;
            padding:0px;
            background-color: #A8C131;
            color: white;
            z-index:0;
            padding:20px;
        }
        .simpleqcmtitle h2{
            color: white;
        }
        .hideheader {
            display:none;
        }
    </style>
EOT;
    return $game;
}
